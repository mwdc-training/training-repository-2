<?php

function mwdc_enqueues_function(){
    
    /*styles*/
    wp_enqueue_style('portfolio_css', PLUGIN_DIR_URL.'css/mwdc_portfolio.css');
    
    /*js*/
    wp_register_script('imagesloaded', PLUGIN_DIR_URL.'js/imagesloaded.pkgd.min.js', array('jquery'), '1.0.0', 1);
    wp_register_script('isotope', PLUGIN_DIR_URL.'js/isotope.pkgd.min.js', array('jquery', 'imagesloaded'), '1.0.0', 1);
    wp_enqueue_script('portfolio_js', PLUGIN_DIR_URL.'js/mwdc_portfolio.js', array('jquery', 'isotope'), '1.0.0', 1);
}

add_action('wp_enqueue_scripts', 'mwdc_enqueues_function');