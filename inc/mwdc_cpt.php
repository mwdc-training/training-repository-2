<?php

function my_custom_posttypes() {
    
    $labels = array(
        'name'               => 'Porfolio',
        'singular_name'      => 'Porfolio',
        'menu_name'          => 'Porfolio',
        'name_admin_bar'     => 'Porfolio',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Porfolio',
        'new_item'           => 'New Porfolio',
        'edit_item'          => 'Edit Porfolio',
        'view_item'          => 'View Porfolio',
        'all_items'          => 'All Porfolio',
        'search_items'       => 'Search Porfolio',
        'parent_item_colon'  => 'Parent Porfolio:',
        'not_found'          => 'No testimonials found.',
        'not_found_in_trash' => 'No testimonials found in Trash.',
    );
    
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_icon'          => 'dashicons-id-alt',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'portfolio' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'supports'           => array( 'title', 'editor', 'thumbnail' )
    );
    register_post_type( 'portfolio', $args );
}
add_action( 'init', 'my_custom_posttypes' );

// Flush rewrite rules to add "review" as a permalink slug
function my_rewrite_flush() {
    my_custom_posttypes();
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'my_rewrite_flush' );