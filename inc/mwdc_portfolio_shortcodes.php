<?php

/*
    mwdc_portfolio shortcode
    
    * lists portfolio items
*/
function mwdc_portfolio_function(){ ob_start();
    
    wp_reset_query();
    
    $args = array(
        'post_type' => 'portfolio',
        'posts_per_page' => -1,
        'post_status' => 'publish'
    );
    
    $portfolios = new WP_Query( $args );
    
    if( $portfolios->have_posts() ){ ?>
        
        <div id="mwdc_portfolio_wrapper" class="grid">
            
            <?php while( $portfolios->have_posts() ){

                $portfolios->the_post();

                mwdc_get_template_part( 'content-loop-portfolio', true );

            } ?>
            
        </div> <!-- #mwdc_portfolio_wrapper -->
    <?php }
                                   
    $content = ob_get_clean();
                                   
    return $content;
}

add_shortcode('mwdc_portfolio', 'mwdc_portfolio_function');