<?php
    $portfolio_url = get_post_meta( $id, 'portfolio_item_link', true );
    $is_404 = test_url_against_404($portfolio_url);

    if( $is_404 ){
        return false;
    }

    $portfolio_img = get_the_post_thumbnail( $id, 'portfolio_media' );
?>
<div class="portfolio_item grid-item">
    <div class="portfolio_item_inner">
        <a href="<?php echo $portfolio_url; ?>" target="_blank">
            <?php if( isset($portfolio_img) && $portfolio_img ){ echo $portfolio_img; } ?>
            
            <div class="toggled_content">
                <h2><?php echo get_the_title() ?></h2>
            </div>
        </a>
    </div>
</div>