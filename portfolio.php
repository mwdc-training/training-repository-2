<?php
/*
Plugin Name: Portfolio
Description: Add portfolio interface
Version:     1.0.0
Author:      Mostafa Shenawy
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

/*constants*/
define('PLUGIN_DIR_PATH', plugin_dir_path( __FILE__ ));
define('PLUGIN_DIR_URL', plugin_dir_url( __FILE__ ));

/*includes*/
include_once 'inc/helper_functions.php';
include_once 'inc/mwdc_plugin_template_loader.php';
include_once 'inc/mwdc_enqueues.php';
include_once 'inc/mwdc_cpt.php';
include_once 'inc/mwdc_portfolio_shortcodes.php';

/*portfolio media size*/
add_action( 'after_setup_theme', 'mwdc_portfolio_media' );
function mwdc_portfolio_media() {
    add_image_size( 'portfolio_media', 360, 270, true ); // 300 pixels wide (and unlimited height)
}