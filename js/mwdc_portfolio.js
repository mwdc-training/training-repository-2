jQuery( function($){
    
    /*
        portfolio loop
        
        * initaite isotope grid
    */
    if( $('#mwdc_portfolio_wrapper.grid') ){
        var $grid = $('#mwdc_portfolio_wrapper.grid');

        var $grid_object = $($grid).isotope({
            // options
            itemSelector: '.grid-item',
            layoutMode: 'masonry',
            masonry: {
                isFitWidth: true,
                gutter: 15
            }
        });

        // layout Isotope again after all images have loaded
        $grid_object.imagesLoaded().done(function(){
            $grid_object.isotope('layout');
        });
        
        //articles same height
        function articles_same_height(){
            var $container = $grid,
                $container_width = $container.width(),
                $item_width = $container.find(' > .grid-item').width(),
                grid_cols = Math.floor( $container_width/$item_width );
            
            var divs = $container.find(' > .grid-item');
            
            for(var i = 0; i < divs.length; i+=grid_cols) {
              divs.slice(i, i+grid_cols).wrapAll("<div class='new'></div>");
            }
            
            var maxHeight = 0;
            $('.new').each(function(i, e){
                $(e).find('.grid-item').each(function(){
                   if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
                });

                $(e).find('.grid-item').height(maxHeight + 2);
            })
            
        }
        
        articles_same_height();
        
        $(window).resize(function(){
            articles_same_height();
        })
        
    }
    
} )